import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConnectionStorageService {

  public favoriteTrains: Array<number> = new Array<number>();

  constructor() { }
}
