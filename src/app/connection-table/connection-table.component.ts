import { Component, OnInit } from '@angular/core';
import {ConnectionService} from './service/connection.service';
import {ConnectionStorageService} from './storage/connection-storage.service';

@Component({
  selector: 'app-connection-table',
  templateUrl: './connection-table.component.html',
  styleUrls: ['./connection-table.component.css']
})
export class ConnectionTableComponent implements OnInit {

  private connectionData: any;

  constructor(
    private connectionService: ConnectionService,
    private storageService: ConnectionStorageService
  ) { }

  ngOnInit(): void {
    this.connectionService.getBernToZurichConnectionsObservable().subscribe(
      data => {
        this.connectionData = data;
      }
    );
  }
  public saveFavorit(id: number): void {
    console.log('Saved Number: ', id);
    this.storageService.favoriteTrains.push(id);
  }
  public getAllFavorites(): void {
    console.log(this.storageService.favoriteTrains.toString());
  }
}
