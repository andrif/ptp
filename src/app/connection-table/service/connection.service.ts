import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getBernToZurichConnectionsObservable(): Observable<any> {
    return this.httpClient.get('http://transport.opendata.ch/v1/connections?from=Bern&to=Zurich&limit=10');
  }
}
